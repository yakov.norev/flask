import logging
import os

logger = logging.getLogger(__name__)


class DevelopmentConfig(object):
    BASE_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
    SQLALCHEMY_DATABASE_URI = f"sqlite:///{BASE_DIR}/db.sqlite"

    SQLALCHEMY_MIGRATE_REPO = os.path.join(BASE_DIR, 'migrations')
    HOST = "localhost"
    PORT = "5000"

    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
